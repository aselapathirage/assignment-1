#include <stdio.h>
/* Question 3: Program to Reverse a Number */

int main(){
    int num,reverse=0;
    printf("Enter a number: ");
    scanf("%d", &num);

    do{
        int r=num%10;
        reverse=reverse*10+r;
        num/=10;
    }while(num!=0);

    printf("Number in reverse: %d", reverse);

    return 0;
}
