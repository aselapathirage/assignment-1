#include <stdio.h>
/* Question 1: program to calculate the sum of all given positive integers until we enter the 0 or a negative value */

int main(){
    int num=0,total=0;

    do{
        printf("Please enter a positive integer(zero or negative to stop executing): ");
        scanf("%d", &num);
        if(num<=0) break;
        total+=num;
    }while(num>0);

    printf("Total : %d\n",total);

    return 0;
}
