#include <stdio.h>
/* Question 2: program to determine the given number is a prime number */

int main(){
    int num,count=0;
    printf("Enter a number: ");
    scanf("%d", &num);

    for(int i=2;i<=num/2;i++){
        if(num%i==0) {count++;}
    }

    if(num>1 && count==0){
        printf("%d is a Prime number\n",num);
    }else {
        printf("%d is not a Prime number\n",num);
    }
    return 0;
}
