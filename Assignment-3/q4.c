#include <stdio.h>
/* Question 4: program to find factors of a given number */

int main(){
    int num=0;
    printf("Enter a number: ");
    scanf("%d", &num);

    printf("Factors of %d : ", num);
    /* loop to print the factors */
    int i=1;
    while(i<=num){
        if(num%i==0){
            printf("%d ",i);
        }
        i++;
    }

    printf("\n");

    return 0;
}
