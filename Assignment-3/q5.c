#include <stdio.h>
/* Question 5: Program to print multiplication tables */

int main(){
    int n;
    printf("Enter an positive integer: ");
    scanf("%d", &n);

    for(int i=1;i<=n;i++){
        printf("Multiplication table of %d \n\n",i);
        for(int j=1;j<=10;j++){
            printf("%d * %d = %d\n", i, j, i*j);
        }
        printf("------------------\n\n");
    }


    return 0;
}
