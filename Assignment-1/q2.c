#include <stdio.h>
/* Question 2: Computes the area of a disk*/

int main(){
    float rad, area;
    const float PI=3.14;
    printf("Enter radius : ");
    scanf("%f", &rad);
    area= PI*rad*rad;
    printf("Area of the disk: %.4f \n", area);
    return 0;
}
