#include <stdio.h>
/* Question 1: Read and multiply 2 floating point numbers */

int main(){
    float num1, num2, ans;
    printf("Enter number 1 : ");
    scanf("%f", &num1);
    printf("Enter number 2 : ");
    scanf("%f", &num2);
    ans= num1*num2;
    printf("Answer :  %.2f x %.2f = %.4f \n", num1, num2,ans);
    return 0;
}
