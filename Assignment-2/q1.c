#include <stdio.h>
#include <stdlib.h>
/* Question 1: Checking whether the input number is either positive or negative or zero */

int main(){
    double num;
    printf("Enter a number: ");
    scanf("%lf", &num);
    if(num<0){
        printf("Negative number\n");
    }else if(num>0){
        printf("Positive number\n");
    }else printf("Zero\n");

    return 0;
}
