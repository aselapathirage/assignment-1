#include <stdio.h>
#include <stdlib.h>

/* Question 3: Checking whether the input character is vowel or consonant */

int main(){
    char ch, character;
    printf("Enter a character: ");
    scanf("%c", &ch);
    /* condition to check whether the character is Alphabetical */
    if((ch>='A' && ch<='Z')||(ch>='a' && ch<='z')){
        /* condition to check whether the character is capital or simple */
        if(ch>='A' && ch<='Z'){
            character=ch+32;
        }else{
            character=ch;
        }
        /* check whether the character is a vowel or a consonant */
        switch(character){
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u': printf("%c is a Vowel\n",ch);break;
            default: printf("%c is a Consonant\n",ch);break;
        }
    }else {
        printf("%c is not an Alphabetical character\n",ch);
    }

    return 0;
}
